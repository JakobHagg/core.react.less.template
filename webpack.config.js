const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
   mode: 'development',
   entry: { 'main': './wwwroot/source/app.jsx' },
   output: {
      path: path.resolve(__dirname, 'wwwroot/dist'),
      filename: 'bundle.js',
      publicPath: 'dist/',
      sourceMapFilename: 'bundle.map'
   },
   resolve: {
      extensions: [".webpack.js", ".web.js", ".js", ".json", ".jsx"]
   },
   module: {
      rules: [{
        test: /\.css$/,
        use: [
            { loader: "style-loader" },
            { loader: "file-loader" }
        ]
    }, {
        test: /\.less$/,
        use: [
            { loader: "style-loader" },
            { loader: "css-loader" },
            { loader: "less-loader" }
        ]
    }, {
        test: /\.png$/,
        use: [ { loader: "url-loader?limit=100000" } ]
    }, {
        test: /\.jpg$/,
        use: [ { loader: "file-loader" } ]
    }, {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: [ { loader: "url-loader?limit=10000&mimetype=application/font-woff" } ]
    }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: [ { loader: "url-loader?limit=10000&mimetype=application/octet-stream" } ]
    }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: [ { loader: "file-loader" } ]
    }, {
        test: /\.svg$/,
        exclude: /node_modules/,
        loader: 'svg-react-loader',
        query: {
        classIdPrefix: '[name]-[hash:8]__',
        filters: [ ],
        propsMap: {
        fillRule: 'fill-rule',
        },
        xmlnsTest: /^xmlns.*$/,
        },
       }, 
      {
         test: /\.jsx$/,
         exclude: /(node_modules)/,
         use: {
            loader: 'babel-loader',
            options: {
               presets: ['@babel/preset-react', '@babel/preset-env']
            }
         }
      }
      ]
   },
   devtool: "source-map"
};