import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Button from '@material-ui/core/Button';

import './message.less';
import { Paper } from '@material-ui/core';

export default class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {message:''};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({message: event.target.value});
    }

    render() {
        return (
            <Paper className={styles.paper}>
                <h3 className="message">Message: {this.state.message}</h3>
                Enter message: <br />
                <input type="text" value={this.state.message} onChange={this.handleChange} />
                <Button variant="contained" color="primary">
                    Clear
                </Button>
            </Paper>
        );
    }
}

const styles = theme => ({
    main: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 400,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
      margin: theme.spacing.unit,
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing.unit,
    },
    submit: {
      marginTop: theme.spacing.unit * 3,
    },
  });