import React from 'react';
import ReactDOM from 'react-dom';

import './app.less'

import CssBaseline from '@material-ui/core/CssBaseline';

import Message from './message';
import SignIn from './SignIn';

ReactDOM.render(<div><CssBaseline /><SignIn /></div>, document.getElementById('root'));

module.hot.accept();